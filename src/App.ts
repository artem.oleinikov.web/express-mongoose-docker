import express from 'express';
import { PORT } from './config';
import { errorMiddleware, loggerMiddleware } from './middleware';
import { connectToMongo } from './connectors/mongo';

export default class App {
  public app: express.Application;

  constructor(routers: express.Router[]) {
    this.app = express();

    this._connectToTheDatabase();
    this._initializeDefaultMiddlewares();
    this._initializeDefaultRouters(routers);
    this._initializeErrorHandling();
  }

  public listen() {
    this.app.listen(PORT, () => {
      console.log(`===================================[Server had been started at http://localhost:${PORT}]=============================================`);
    });
  }

  private _initializeDefaultMiddlewares() {
    this.app.use(express.json());
    this.app.use(loggerMiddleware);
  }

  private _initializeErrorHandling() {
    this.app.use(errorMiddleware);
  }

  private _initializeDefaultRouters(routers: express.Router[]) {
    this.app.use(routers);
  }

  private _connectToTheDatabase() {
    connectToMongo();
  }
}
