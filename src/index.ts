import 'dotenv/config';
import App from './App';
import { routers } from './routers';

const app = new App(routers);

app.listen();
