import { Request, Response, NextFunction } from 'express';

function loggerMiddleware(request: Request, response: Response, next: NextFunction) {
  console.log(`=====================================[loggerMiddleware (OPEN)]===========================================`);
  console.log(`${request.method} ${request.path}`);
  console.log('query:', request.query);
  console.log('body:', request.body);
  console.log(`=====================================[loggerMiddleware (CLOSE)]==========================================`);
  next();
}

export default loggerMiddleware;
