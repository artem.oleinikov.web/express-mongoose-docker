import { MONGODB } from '../config';
import { connect } from 'mongoose';

export const connectToMongo = async () => {
  await connect(MONGODB.connectionString);
  console.log(`===================================[Connected to database]=============================================`);
};
