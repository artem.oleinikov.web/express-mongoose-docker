import { connect } from 'amqplib';
import { RABBIT } from '../config';

export class RabbitConnect {
  private _uri: string;
  private _connection: any;
  private _chanel: any;

  constructor() {
    // Строка подключения к rabbit будет браться из окружения
    this._uri = RABBIT.uri;
  }

  protected async connect() {
    this._connection = await connect(this._uri);
    this._chanel = await this._connection.createChannel();
  }

  protected async disconnect() {
    await this._chanel.close();
    return this._connection.close();
  }

  protected get chanel() {
    return this._chanel;
  }
}
