export const PORT = process.env.PORT || 5042;

export const MONGODB = {
  connectionString: process.env.MONGODB_URL || 'mongodb://database:27017/rabbit-mq',
  nameDatabase: process.env.MONGODB_NAME_DATABASE || 'rabbit-mq',
};

export const RABBIT = {
  uri: process.env.RABBIT_URI || 'amqp://localhost'
}
