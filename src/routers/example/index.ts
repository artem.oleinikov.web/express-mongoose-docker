import { Router } from 'express';
import { ExampleController } from '../../controllers/example';

export const exampleRouters = Router();

exampleRouters.get('/examples', ExampleController.getAll);
exampleRouters.get('/example/:id', ExampleController.findOneById);
exampleRouters.post('/example', ExampleController.create);
exampleRouters.patch('/example/:id', ExampleController.update);
