import { Router } from 'express';
// routers
import { exampleRouters } from './example';

export const routers = Router();

routers.use(exampleRouters);
