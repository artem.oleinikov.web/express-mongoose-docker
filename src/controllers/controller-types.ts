import { NextFunction, Request, Response } from 'express';
import * as core from 'express-serve-static-core';

export type IController<ResBody = {}, ReqBody = {}, ReqQuery = {}> =
  (req: Request<core.ParamsDictionary, ResBody, ReqBody, ReqQuery>, res: Response<ResBody>, next: NextFunction) => void
