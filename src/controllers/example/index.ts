import ExampleServices from '../../services/example';

// DTO
import { CreateExampleDto, UpdateExampleDto } from '../../services/example/dto';

// Types
import { IExample } from '../../models/Example';
import { IController } from '../controller-types';

type TExampleController = {
  getAll: IController<IExample[]>,
  findOneById: IController<IExample>,
  create: IController<IExample, CreateExampleDto>,
  update: IController<IExample, UpdateExampleDto>,
}

const service = new ExampleServices();

export const ExampleController: TExampleController = {
  getAll: async (req, res, next) => {
    // TODO add pagination to query
    console.log('[ExampleController] getAll');
    const result = await service.getAll();
    res.send(result);
  },

  findOneById: async (req, res, next) => {
    try {
      // TODO add body validation
      console.log('[ExampleController] findOneById: req.params.id', req.params.id);
      const result = await service.findOneById(req.params.id);
      res.send(result);
    } catch (error) {
      console.log('ExampleController', error);
      next(error)
    }
  },

  create: async (req, res, next) => {
    // TODO add body validation
    console.log('[ExampleController] create: req.body');
    const result = await service.create(req.body);
    res.send(result);
  },

  update: async (req, res, next) => {
    // TODO add body validation
    console.log('[ExampleController] update: req.params.id', req.params.id);
    const result = await service.update({ _id: req.params.id, ...req.body });
    res.send(result);
  }
};

