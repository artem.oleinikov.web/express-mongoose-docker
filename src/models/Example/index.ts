import { model, Schema } from 'mongoose';

export interface IExample {
  _id: string,
  title: string,
  description: string,
  value: number,
}

const ExampleSchema = new Schema<IExample>({
  title: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
  value: {
    type: Number,
    required: true,
    max: 100,
    min: 0
  }
});

export default model<IExample>('Example', ExampleSchema);
