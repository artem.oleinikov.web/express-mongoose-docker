export type UpdateExampleDto = {
  title?: string,
  description?: string,
  value?: number,
}

