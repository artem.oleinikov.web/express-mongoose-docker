import { CreateExampleDto } from './createExample.dto';
import { UpdateExampleDto } from './updateExample.dto';

export {
  CreateExampleDto,
  UpdateExampleDto
};
