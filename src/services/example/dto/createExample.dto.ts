import { IExample } from '../../../models/Example';

export type CreateExampleDto = Omit<IExample, '_id'>
