import ExampleSchema, { IExample } from '../../models/Example';
import ExampleNotFoundException from '../../exceptions/example/ExampleNotFoundException';
import { CreateExampleDto, UpdateExampleDto } from './dto';

export default class ExampleServices {

  async getAll(): Promise<IExample[]> {
    return ExampleSchema.find();
  }

  async findOneById(id: string) {
    const example = await ExampleSchema.findById(id).exec()
    if (!example) throw new ExampleNotFoundException(id);
    return example;
  }

  async create(createDto: CreateExampleDto): Promise<IExample> {
    // TODO add some validation
    const example = await ExampleSchema.create(createDto);
    if (example) await example.save();
    return example;
  }

  async update({ _id, ...payload }: UpdateExampleDto & { _id: string }): Promise<IExample | null> {
    // TODO add some validation
    const example = await ExampleSchema.findOneAndUpdate({ _id }, { $set: payload }).exec();
    return example;
  }
}
